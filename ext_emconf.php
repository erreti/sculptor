<?php

/**
 * Extension Manager/Repository config file for ext: "sculptor"
 */
$EM_CONF[$_EXTKEY] = [
    'title' => 'Sculptor Site Template',
    'description' => '',
    'category' => 'templates',
    'author' => 'Roberto Torresani',
    'author_email' => 'roberto@torresani.eu',
    'state' => 'beta',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'version' => '4.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '12.4.0-13.4.99',
            'fluid_styled_content' => '12.4.0-13.4.99',
            'rte_ckeditor' => '12.4.0-13.4.99',
        ],
        'conflicts' => [
        ],
    ],
];
